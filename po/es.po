# translation of camorama.HEAD.po to Spanish
# translation of camorama.HEAD.es.po to Spanish
# Spanish translation for camorama.
# Copyright © 2003, 2006 the Free Software Foundation, Inc.
# This file is distributed under the same license as the camorama package.
#
# Pablo Gonzalo del Campo <pablodc@bigfoot.com>, 2003.
# Francisco Javier F. Serrador <serrador@arrakis.es>, 2003.
# Francisco Javier F. Serrador <serrador@cvs.gnome.org>, 2006.
# Mauro Carvalho Chehab <mchehab@infradead.org>, 2018. #zanata
# Emilio Herrera <ehespinosa57@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: camorama 0.20.6\n"
"Report-Msgid-Bugs-To: https://github.com/alessio/camorama\n"
"POT-Creation-Date: 2022-03-24 15:31+0100\n"
"PO-Revision-Date: 2022-11-01 23:19+0000\n"
"Last-Translator: Emilio Herrera <ehespinosa57@gmail.com>\n"
"Language-Team: Spanish <https://translate.fedoraproject.org/projects/"
"camorama/master/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.14.1\n"

#: camorama.appdata.xml.in:6
msgid "Camorama"
msgstr "Camorama"

#: camorama.appdata.xml.in:7 camorama.desktop.in:4
msgid "Camorama Webcam Viewer"
msgstr "Visor de cámara web Camorama"

#: camorama.appdata.xml.in:9
msgid ""
"Camorama is a simple webcam viewer, with the ability to apply some video "
"effects."
msgstr ""
"Camorama es un sencillo visualizador de cámara web con la capacidad de "
"aplicar algunos efectos de vídeo."

#: camorama.desktop.in:5 src/callbacks.c:530
msgid "View, alter and save images from a webcam"
msgstr "Ver, alterar y guardar imágenes desde una cámara web"

#: camorama.desktop.in:10
msgid "camorama;webcam;viewer;video-effects;"
msgstr "camorama;webcam;viewer;video-effects;"

#: data/camorama-gtk3.ui:57 data/camorama-gtk4.ui:49
msgid "Camorama-libglade"
msgstr "Camorama-libglade"

#: data/camorama-gtk3.ui:83 data/camorama-gtk4.ui:75
msgid "_Take Picture"
msgstr "_Tomar una Fotografía"

#: data/camorama-gtk3.ui:94 data/camorama-gtk4.ui:86
msgid "Toggle _Full Screen"
msgstr "Cambiar _A Pantalla Completa"

#: data/camorama-gtk3.ui:149 data/camorama-gtk4.ui:141
msgid "_View"
msgstr "_Ver"

#: data/camorama-gtk3.ui:158 data/camorama-gtk3.ui:337
#: data/camorama-gtk4.ui:150 data/camorama-gtk4.ui:329
msgid "Show Adjustments"
msgstr "Mostrar los ajustes"

#: data/camorama-gtk3.ui:168 data/camorama-gtk4.ui:160
msgid "Show Effects"
msgstr "Mostrar efectos"

#: data/camorama-gtk3.ui:177 data/camorama-gtk4.ui:169
msgid "_Change Camera"
msgstr "_Cambiar Cámara"

#: data/camorama-gtk3.ui:192 data/camorama-gtk4.ui:184
msgid "Small"
msgstr "Pequeño"

#: data/camorama-gtk3.ui:357
msgid "Show Controls"
msgstr "Mostrar Controles"

#: data/camorama-gtk3.ui:396 data/camorama-gtk4.ui:381
msgid "Full Screen"
msgstr "Pantalla Completa"

#: data/camorama-gtk3.ui:441 data/camorama-gtk4.ui:428
msgid "Take Picture"
msgstr "Tomar una fotografía"

#: data/camorama-gtk3.ui:536 data/camorama-gtk4.ui:523
msgid "Contrast:"
msgstr "Contraste:"

#: data/camorama-gtk3.ui:548 data/camorama-gtk4.ui:535
msgid "Brightness:"
msgstr "Brillo:"

#: data/camorama-gtk3.ui:560 data/camorama-gtk4.ui:547
msgid "Color:"
msgstr "Color:"

#: data/camorama-gtk3.ui:572 data/camorama-gtk4.ui:559
msgid "Hue:"
msgstr "Tonalidad:"

#: data/camorama-gtk3.ui:584 data/camorama-gtk4.ui:571
msgid "White Balance:"
msgstr "Balance de blancos:"

#: data/camorama-gtk3.ui:666 data/camorama-gtk4.ui:653
msgid "Zoom:"
msgstr "Zoom:"

#: data/camorama-gtk3.ui:714
msgid "Threshold:"
msgstr "Umbral:"

#: data/camorama-gtk3.ui:748
msgid "Channel Threshold:"
msgstr "Umbral del Canal:"

#: data/camorama-gtk3.ui:865 data/camorama-gtk4.ui:784
msgid "<span weight=\"bold\">General</span>"
msgstr "<span weight=\"bold\">General</span>"

#: data/camorama-gtk3.ui:899 data/camorama-gtk4.ui:818
msgid "Automatic Capture"
msgstr "Captura automática"

#: data/camorama-gtk3.ui:921 data/camorama-gtk3.ui:1059
#: data/camorama-gtk3.ui:1098 data/camorama-gtk3.ui:1291
#: data/camorama-gtk3.ui:1410 data/camorama-gtk3.ui:1448
#: data/camorama-gtk3.ui:1686 data/camorama-gtk3.ui:1805
#: data/camorama-gtk3.ui:1843 data/camorama-gtk4.ui:840
#: data/camorama-gtk4.ui:978 data/camorama-gtk4.ui:1017
#: data/camorama-gtk4.ui:1210 data/camorama-gtk4.ui:1329
#: data/camorama-gtk4.ui:1367 data/camorama-gtk4.ui:1605
#: data/camorama-gtk4.ui:1724 data/camorama-gtk4.ui:1762
msgid "     "
msgstr "     "

#: data/camorama-gtk3.ui:944 data/camorama-gtk4.ui:863
msgid "Capture Interval (in minutes):"
msgstr "Intervalo de la captura (en minutos):"

#: data/camorama-gtk3.ui:1016 data/camorama-gtk4.ui:935
msgid "General"
msgstr "General"

#: data/camorama-gtk3.ui:1041 data/camorama-gtk4.ui:960
msgid "<span weight=\"bold\">Local Capture</span>"
msgstr "<span weight=\"bold\">Captura local</span>"

#: data/camorama-gtk3.ui:1075 data/camorama-gtk4.ui:994
msgid "_Enable local capture"
msgstr "H_abilitar la captura local"

#: data/camorama-gtk3.ui:1123 data/camorama-gtk4.ui:1042
msgid "_Directory for captured pics:"
msgstr "_Directorio para las imágenes capturadas:"

#: data/camorama-gtk3.ui:1136 data/camorama-gtk4.ui:1055
msgid "_Filename for pictures:"
msgstr "Nombre de archivo para las _imágenes:"

#: data/camorama-gtk3.ui:1152 data/camorama-gtk4.ui:1071
msgid "Select a Directory"
msgstr "Seleccionar un Directorio"

#: data/camorama-gtk3.ui:1197 data/camorama-gtk3.ui:1593
#: data/camorama-gtk4.ui:1116 data/camorama-gtk4.ui:1512
msgid "Append time to filename"
msgstr "Agregar la hora al nombre del archivo"

#: data/camorama-gtk3.ui:1214 data/camorama-gtk3.ui:1609
#: data/camorama-gtk4.ui:1133 data/camorama-gtk4.ui:1528
msgid "Add a timestamp to captured images"
msgstr "Agregar una marca de tiempo a las imágenes capturadas"

#: data/camorama-gtk3.ui:1273 data/camorama-gtk3.ui:1668
#: data/camorama-gtk4.ui:1192 data/camorama-gtk4.ui:1587
msgid "<span weight=\"bold\">Image Type</span>"
msgstr "<span weight=\"bold\">Tipo de imagen</span>"

#: data/camorama-gtk3.ui:1306 data/camorama-gtk3.ui:1701
#: data/camorama-gtk4.ui:1225 data/camorama-gtk4.ui:1620
msgid "Save files in jpeg format"
msgstr "Guardar los archivos en el formato jpeg"

#: data/camorama-gtk3.ui:1323 data/camorama-gtk3.ui:1718
#: data/camorama-gtk4.ui:1242 data/camorama-gtk4.ui:1637
msgid "Save files in png format"
msgstr "Guardar los archivos en el formato png"

#: data/camorama-gtk3.ui:1368 data/camorama-gtk4.ui:1287
msgid "Local Capture"
msgstr "Captura local"

#: data/camorama-gtk3.ui:1392 data/camorama-gtk4.ui:1311
msgid "<span weight=\"bold\">Remote Capture</span>"
msgstr "<span weight=\"bold\">Captura remota</span>"

#: data/camorama-gtk3.ui:1426 data/camorama-gtk4.ui:1345
msgid "_Enable remote capture"
msgstr "_Habilitar la captura remota"

#: data/camorama-gtk3.ui:1475 data/camorama-gtk4.ui:1394
msgid "Server:"
msgstr "Servidor:"

#: data/camorama-gtk3.ui:1487 data/camorama-gtk4.ui:1406
msgid "Type:"
msgstr "Tipo"

#: data/camorama-gtk3.ui:1499 data/camorama-gtk4.ui:1418
msgid "Save directory:"
msgstr "Directorio para guardar:"

#: data/camorama-gtk3.ui:1511 data/camorama-gtk4.ui:1430
msgid "Filename:"
msgstr "Nombre del archivo:"

#: data/camorama-gtk3.ui:1524 data/camorama-gtk4.ui:1443
msgid "ftp.yourdomain.org"
msgstr "ftp.sudominio.org"

#: data/camorama-gtk3.ui:1563 data/camorama-gtk4.ui:1482
msgid "webcamshot"
msgstr "Instantánea con cámara web"

#: data/camorama-gtk3.ui:1763 data/camorama-gtk4.ui:1682
msgid "Remote Capture"
msgstr "Captura remota"

#: data/camorama-gtk3.ui:1787 data/camorama-gtk4.ui:1706
msgid "<span weight=\"bold\">Timestamp</span>"
msgstr "<span weight=\"bold\">Marca de tiempo</span>"

#: data/camorama-gtk3.ui:1821 data/camorama-gtk4.ui:1740
msgid "Use custom string"
msgstr "Utilizar una cadena personalizada"

#: data/camorama-gtk3.ui:1866 data/camorama-gtk4.ui:1785
msgid "String:"
msgstr "Cadena:"

#: data/camorama-gtk3.ui:1911 data/camorama-gtk4.ui:1830
msgid "Draw date and time"
msgstr "Dibujar la fecha y la hora"

#: data/camorama-gtk3.ui:1955 data/camorama-gtk4.ui:1874
msgid "Timestamp"
msgstr "Marca de tiempo"

#: data/camorama-gtk3.ui:2022 data/camorama-gtk4.ui:1941
msgid "Select a video device to be used"
msgstr "Seleccionar un dispositivo de vídeo para usar"

#: data/org.gnome.camorama.gschema.xml.in:6
msgid "Append timestamp to filename"
msgstr "Agregar una marca de tiempo al nombre de archivo"

#: data/org.gnome.camorama.gschema.xml.in:7
msgid "Appends a timestamp, in UNIX time, to the ends of filenames"
msgstr ""
"Agrega una marca de tiempo, en formato de tiempo UNIX, al final de los "
"nombres de archivos"

#: data/org.gnome.camorama.gschema.xml.in:11
msgid "Toggles auto-capture"
msgstr "Activar la captura automática"

#: data/org.gnome.camorama.gschema.xml.in:12
msgid "Toggles automatic capturing of pictures."
msgstr "Activar la captura automática de imágenes."

#: data/org.gnome.camorama.gschema.xml.in:16
msgid "Interval for auto-capture"
msgstr "Intervalo para la captura automática"

#: data/org.gnome.camorama.gschema.xml.in:17
msgid "Specifies the interval for automatic capture of pictures."
msgstr "Especifica el intervalo para la captura automática de imágenes."

#: data/org.gnome.camorama.gschema.xml.in:21
msgid "Video device"
msgstr "Dispositivo de vídeo"

#: data/org.gnome.camorama.gschema.xml.in:22
msgid "The unix file system path to the device that's assigned to your camera."
msgstr ""
"La ruta del sistema de archivos unix al dispositivo que está asignado a su "
"cámara."

#: data/org.gnome.camorama.gschema.xml.in:26
msgid "Toggles the datestamp in captures"
msgstr "Activar la marca de fecha en las capturas"

#: data/org.gnome.camorama.gschema.xml.in:27
msgid "Toggles the drawing of a datestamp within the captured pictures"
msgstr ""
"Activar el dibujado de la marca de fecha dentro de las imágenes capturadas"

#: data/org.gnome.camorama.gschema.xml.in:31
msgid "Local capture image format"
msgstr "Formato de la imagen de la captura local"

#: data/org.gnome.camorama.gschema.xml.in:32
msgid "Specifies the image format to be used for locally saved pictures"
msgstr ""
"Especifica el formato de imagen a ser utilizado para las imágenes guardadas "
"localmente"

#: data/org.gnome.camorama.gschema.xml.in:36
msgid "Image height"
msgstr "Altura de la imagen"

#: data/org.gnome.camorama.gschema.xml.in:37
msgid "Image height used by the camera. Zero means autoselect"
msgstr ""
"Altura de la imagen usada por la cámara. Cero significa selección automática"

#: data/org.gnome.camorama.gschema.xml.in:41
#, fuzzy
msgid "Hostname for the server"
msgstr "Nombre de equipo para el servidor FTP"

#: data/org.gnome.camorama.gschema.xml.in:42
#, fuzzy
msgid "Hostname for the server used for remote captures."
msgstr "Nombre de equipo para el servidor FTP para las capturas remotas."

#: data/org.gnome.camorama.gschema.xml.in:46
msgid "Toggles local capture"
msgstr "Activar la captura local"

#: data/org.gnome.camorama.gschema.xml.in:47
msgid ""
"When capturing pictures via the \"Take Picture\" button, or with auto "
"capture, this key toggles the saving of pictures locally."
msgstr ""
"Al capturar imágenes por medio del botón «Tomar fotografía», o con la "
"captura automática, esta clave activa el guardado de las imágenes localmente."

#: data/org.gnome.camorama.gschema.xml.in:51
#, fuzzy
msgid "Protocol to use for remote access"
msgstr "Carpeta para las capturas remotas"

#: data/org.gnome.camorama.gschema.xml.in:52
msgid ""
"The URI name of the protocol that will be used to connect to a remote "
"server. Can be ftp, sftp or smb"
msgstr ""

#: data/org.gnome.camorama.gschema.xml.in:56
msgid "Complete URI used to store image snapshots remotely"
msgstr ""

#: data/org.gnome.camorama.gschema.xml.in:57
msgid ""
"The complete URI of the last successful access to a remote server, including "
"protocol, host and remote directory"
msgstr ""

#: data/org.gnome.camorama.gschema.xml.in:61
msgid "Append timestamp to remote filename"
msgstr "Agregar una marca de tiempo al nombre de archivo remoto"

#: data/org.gnome.camorama.gschema.xml.in:62
msgid "Appends a timestamp, in UNIX time, to the ends of remote filenames"
msgstr ""
"Agrega una marca de tiempo, en formato de tiempo UNIX, al final de los "
"nombres de archivos remotos"

#: data/org.gnome.camorama.gschema.xml.in:66
msgid "Toggles remote capture"
msgstr "Activar la captura remota"

#: data/org.gnome.camorama.gschema.xml.in:67
msgid ""
"When capturing pictures via the \"Take Picture\" button, or with auto "
"capture, this key toggles the saving of pictures to a remote server."
msgstr ""
"Al capturar imágenes por medio del botón «Tomar fotografía», o con la "
"captura automática, esta clave activa el guardado de las imágenes en un "
"servidor remoto."

#: data/org.gnome.camorama.gschema.xml.in:71
msgid "Remote capture image format"
msgstr "Formato de la imagen de la captura remota"

#: data/org.gnome.camorama.gschema.xml.in:72
msgid "Specifies the image format to be used for remotely saved pictures"
msgstr ""
"Especifica el formato de imagen a ser utilizado para las imágenes guardadas "
"remotamente"

#: data/org.gnome.camorama.gschema.xml.in:76
msgid "Folder for remote captures"
msgstr "Carpeta para las capturas remotas"

#: data/org.gnome.camorama.gschema.xml.in:77
msgid "Folder for saving captured pictures remotely"
msgstr "Carpeta para guardar las imágenes capturadas remotamente"

#: data/org.gnome.camorama.gschema.xml.in:81
msgid "Remote save filename"
msgstr "Nombre de archivo para guardar remotamente"

#: data/org.gnome.camorama.gschema.xml.in:82
msgid "Name for the filename of uploaded images."
msgstr "Nombre del archivo de la imágenes subidas."

#: data/org.gnome.camorama.gschema.xml.in:86
msgid "Toggles timestamp for remote captures"
msgstr "Activar la marca de tiempo para las capturas remotas"

#: data/org.gnome.camorama.gschema.xml.in:87
msgid "Toggles the insertion of a timestamp into the images for FTP upload."
msgstr ""
"Activar la inserción de la marca de tiempo en la imágenes a enviar al "
"servidor FTP."

#: data/org.gnome.camorama.gschema.xml.in:91
msgid "Folder for local captures"
msgstr "Carpeta para las capturas locales"

#: data/org.gnome.camorama.gschema.xml.in:92
msgid "Folder for saving captured pictures to locally"
msgstr "Carpeta para guardar las imágenes capturadas localmente"

#: data/org.gnome.camorama.gschema.xml.in:96
msgid "Filename for local captures"
msgstr "Nombre de archivo para las capturas locales"

#: data/org.gnome.camorama.gschema.xml.in:97
msgid "Filename for saving captured pictures to locally"
msgstr "Nombre de archivo para guardar las imágenes capturadas localmente"

#: data/org.gnome.camorama.gschema.xml.in:101
msgid "Toggles timestamp for local captures"
msgstr "Activar la marca de tiempo para las capturas locales"

#: data/org.gnome.camorama.gschema.xml.in:102
msgid "Toggles the insertion of a timestamp into local images"
msgstr "Activar la inserción de la marca de tiempo en las imágenes locales"

#: data/org.gnome.camorama.gschema.xml.in:106
msgid "Custom text for the timestamp"
msgstr "Texto personalizado para la marca de tiempo"

#: data/org.gnome.camorama.gschema.xml.in:107
msgid "Defines a string of custom text to be inserted into the image."
msgstr ""
"Define una cadena de texto personalizado para ser insertada en la imagen."

#: data/org.gnome.camorama.gschema.xml.in:111
msgid "Toggles the custom string"
msgstr "Activar la cadena personalizada"

#: data/org.gnome.camorama.gschema.xml.in:112
msgid "Toggles the use of the custom string for the timestamp in images."
msgstr ""
"Activar el uso de la cadena personalizada para la marca de tiempo en las "
"imágenes."

#: data/org.gnome.camorama.gschema.xml.in:116
msgid "Toggles the video adjustment sliders"
msgstr "Activar los deslizadores de ajuste de vídeo"

#: data/org.gnome.camorama.gschema.xml.in:117
msgid "Toggles the use of the sliders to adjust the video properties"
msgstr ""
"Activar el uso de los deslizadores para ajustar las propiedades del vídeo"

#: data/org.gnome.camorama.gschema.xml.in:121
msgid "Toggles the video effects buttons"
msgstr "Conmutar los botones de efectos de vídeo"

#: data/org.gnome.camorama.gschema.xml.in:122
msgid "Toggles the use of the buttons to adjust the video filters"
msgstr "Conmutar el uso de los botones para ajustar los filtros de vídeo"

#: data/org.gnome.camorama.gschema.xml.in:126
#, fuzzy
msgid "Image width"
msgstr "ancho de la captura"

#: data/org.gnome.camorama.gschema.xml.in:127
msgid "Image width used by the camera. Zero means autoselect"
msgstr ""

#: src/callbacks.c:531
msgid "translator_credits"
msgstr "Pablo Gonzalo del Campo <pablodc@bigfoot.com>, 2003"

#: src/callbacks.c:644
#, c-format
msgid "%.2f fps - current     %.2f fps - average"
msgstr ""

#: src/callbacks.c:1241
msgid "Can't allocate memory to store devices"
msgstr ""

#: src/callbacks.c:1353
msgid "Didn't find any camera"
msgstr ""

#: src/callbacks.c:1449
#, c-format
msgid "%dx%d (max %.1f fps)"
msgstr ""

#: src/callbacks.c:1453
#, c-format
msgid "%dx%d"
msgstr ""

#: src/camorama-filter-laplace.c:134
msgid "Laplace (4 Neighbours)"
msgstr "Laplace (4 Vecinos)"

#. TRANSLATORS: This is a noun
#: src/camorama-filter-mirror.c:75
msgid "Mirror"
msgstr "Espejo"

#. TRANSLATORS: This is a noun
#: src/camorama-filter-reichardt.c:274
msgid "Reichardt"
msgstr ""

#: src/camorama-window.c:116
msgid "_Add Filter"
msgstr "_Añadir filtro"

#: src/camorama-window.c:203
msgid "Effects"
msgstr "Efectos"

#: src/fileio.c:115 src/fileio.c:282
msgid "Could save temporary image file in /tmp."
msgstr "No se ha podido guardar el archivo temporal de imagen en /tmp."

#: src/fileio.c:132
#, c-format
msgid "Unable to create image '%s'."
msgstr "No se puede crear la imagen «%s»"

#: src/fileio.c:143 src/fileio.c:440
#, c-format
msgid "Could not save image '%s/%s'."
msgstr "No se ha podido guardar la imagen «%s/%s»."

#: src/fileio.c:154
#, fuzzy, c-format
msgid "Could not create a thread to save image '%s/%s'."
msgstr "No se ha podido guardar la imagen «%s/%s»."

#: src/fileio.c:195
#, fuzzy, c-format
msgid "An error occurred mounting %s:%s."
msgstr "Ha ocurrido un error al abrir %s."

#: src/fileio.c:235
#, fuzzy, c-format
msgid "An error occurred accessing %s."
msgstr "Ha ocurrido un error al abrir %s."

#: src/fileio.c:288
#, c-format
msgid ""
"Unable to open temporary image file '%s'.\n"
"Cannot upload image."
msgstr ""
"No se puede abrir el archivo de imagen temporal «%s».\n"
"No se puede enviar la imagen."

#: src/fileio.c:320
#, c-format
msgid "An error occurred opening %s."
msgstr "Ha ocurrido un error al abrir %s."

#: src/fileio.c:331
#, fuzzy, c-format
msgid "An error occurred opening %s for write: %s."
msgstr "Ha ocurrido un error al abrir %s."

#: src/fileio.c:342
#, fuzzy, c-format
msgid "An error occurred writing to %s: %s."
msgstr "Ha ocurrido un error al escribir en %s"

#: src/fileio.c:348
#, fuzzy, c-format
msgid "An error occurred closing %s: %s."
msgstr "Ha ocurrido un error al abrir %s."

#: src/fileio.c:411
#, c-format
msgid "Could not create directory '%s'."
msgstr "No se ha podido crear el directorio «%s»"

#: src/fileio.c:420
#, c-format
msgid "Could not change to directory '%s'."
msgstr "No se ha podido cambiar al directorio «%s»."

#: src/filter.c:96
msgid "Invert"
msgstr "Invertir"

#: src/filter.c:184
msgid "Threshold (Overall)"
msgstr "Umbral (Todo)"

#: src/filter.c:277
msgid "Threshold (Per Channel)"
msgstr "Umbral (por canal)"

#: src/filter.c:355
msgid "Wacky"
msgstr "Chiflado"

#: src/filter.c:461
msgid "Smooth"
msgstr "Suavizado"

#: src/filter.c:497
msgid "Monochrome"
msgstr "Monocromo"

#: src/filter.c:535
msgid "Monochrome (Weight)"
msgstr "Monocromo (Peso)"

#. TRANSLATORS: http://en.wikipedia.org/wiki/Sobel
#: src/filter.c:592
msgid "Sobel"
msgstr "Sobel"

#: src/main.c:31
msgid "show version and exit"
msgstr "muestra la versión y termina"

#: src/main.c:33
msgid "v4l device to use"
msgstr "dispositivo v4l a utilizar"

#: src/main.c:35
msgid "enable debugging code"
msgstr "activar la depuración de código"

#: src/main.c:37
msgid "capture width"
msgstr "ancho de la captura"

#: src/main.c:39
msgid "capture height"
msgstr "altura de la captura"

#: src/main.c:41
msgid "maximum capture size"
msgstr "tamaño máximo de la captura"

#: src/main.c:43
msgid "minimum capture size"
msgstr "tamaño mínimo de la captura"

#: src/main.c:45
msgid "middle capture size"
msgstr "tamaño intermedio de la captura"

#: src/main.c:47
msgid "use read() rather than mmap()"
msgstr "usar read() en vez de  mmap()"

#: src/main.c:49
msgid "disable video scaler"
msgstr ""

#: src/main.c:51 src/main.c:53
#, fuzzy
#| msgid "use read() rather than mmap()"
msgid "use userptr pointer rather than mmap()"
msgstr "usar read() en vez de  mmap()"

#: src/main.c:55
#, fuzzy
#| msgid "v4l device to use"
msgid "v4l device input to use"
msgstr "dispositivo v4l a utilizar"

#: src/main.c:117
#, c-format
msgid ""
"\n"
"\n"
"Camorama version %s\n"
"\n"
msgstr ""
"\n"
"\n"
"Camorama versión %s\n"
"\n"

#: src/main.c:151
#, fuzzy
msgid "Couldn't load builder file"
msgstr "No se ha podido encontrar el archivo de pixmap: %s"

#: src/main.c:180
#, c-format
msgid "%s not found. Falling back to %s"
msgstr ""

#: src/main.c:183
#, c-format
msgid "%s not found."
msgstr ""

#: src/v4l.c:1087
#, fuzzy, c-format
msgid ""
"Could not connect to video device (%s).\n"
"Please check connection. Error: %d"
msgstr ""
"No se ha podido conectar al dispositivo de vídeo (%s).\n"
"Por favor revise la conexión."

#: src/v4l.c:1096
#, c-format
msgid "Device %s is not a video capture device."
msgstr ""

#: src/v4l.c:1270 src/v4l.c:1340
#, c-format
msgid ""
"Could not connect to video device (%s).\n"
"Please check connection."
msgstr ""
"No se ha podido conectar al dispositivo de vídeo (%s).\n"
"Por favor revise la conexión."

#: src/v4l.c:1348
msgid "Device video formats are incompatible with camorama."
msgstr ""

#: src/v4l.c:1364
#, fuzzy, c-format
#| msgid ""
#| "Could not connect to video device (%s).\n"
#| "Please check connection."
msgid ""
"Could not set window info on video device (%s).\n"
"Please check connection."
msgstr ""
"No se ha podido conectar al dispositivo de vídeo (%s).\n"
"Por favor revise la conexión."

#: src/v4l.c:1385
#, c-format
msgid "Could not set format to %c%c%c%c on video device (%s)."
msgstr ""

#: src/v4l.c:1404
#, c-format
msgid "Returned width is zero on video device (%s)!"
msgstr ""

#: src/v4l.c:1428 src/v4l.c:1511
#, c-format
msgid "VIDIOC_REQBUFS  --  could not request buffers (%s), exiting...."
msgstr ""

#: src/v4l.c:1437 src/v4l.c:1520 src/v4l.c:1535
msgid "could not allocate memory for video buffers, exiting...."
msgstr ""

#: src/v4l.c:1453
#, c-format
msgid "VIDIOC_QUERYBUF  --  could not query buffers (%s), exiting...."
msgstr ""

#: src/v4l.c:1468
#, c-format
msgid "failed to memory map buffers (%s), exiting...."
msgstr ""

#: src/v4l.c:1482
#, c-format
msgid "VIDIOC_QBUF  --  could not enqueue buffers (%s), exiting...."
msgstr ""

#: src/v4l.c:1492 src/v4l.c:1559
#, c-format
msgid "failed to start streaming (%s), exiting...."
msgstr ""

#: src/v4l.c:1548
#, c-format
msgid "VIDIOC_QBUF  --  could not query buffers (%s), exiting...."
msgstr ""

#: src/v4l.c:1589 src/v4l.c:1640
#, c-format
msgid "Timeout while waiting for frames (%s)"
msgstr ""

#: src/v4l.c:1703 src/v4l.c:1762
#, c-format
msgid "failed to stop streaming (%s), exiting...."
msgstr ""

#~ msgid "_File"
#~ msgstr "_Archivo"

#~ msgid "_Edit"
#~ msgstr "_Editar"

#, fuzzy
#~ msgid "_Preferences"
#~ msgstr "Preferencias"

#~ msgid "_Help"
#~ msgstr "_Ayuda"

#~ msgid "Preferences"
#~ msgstr "Preferencias"

#~ msgid "camorama"
#~ msgstr "camorama"

#~ msgid "webcam-capture"
#~ msgstr "Captura con cámara web"

#~ msgid "Color Correction"
#~ msgstr "Corrección de color"
